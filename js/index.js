$(function () {
    

   

    //首页图片切换按钮出现与隐藏
    $('.div-img').hover(
        function () {
            $('.left-img').css({'margin-left': '3rem', 'background-color': 'rgba(0, 0, 0, 0.5)'});
            $('.right-img').css({'margin-right': '3rem', 'background-color': 'rgba(0, 0, 0, 0.5)'});
        },
        function () {
            $('.left-img').css({'margin-left': '-4rem', 'background-color': 'rgb(255, 255, 255)'});
            $('.right-img').css({'margin-right': '-4rem', 'background-color': 'rgb(255, 255, 255)'});
        }
    );
    
    
    //LAELIOA主题渐入
    $('.LAELIOAname>div').fadeIn(1000)
    $('.LAELIOAname>h2').fadeIn(1500)

    //首页图片按钮切换图片功能初始化
    var left = $('.left-img');//左按钮
    var right = $('.right-img');//右按钮
    var imgarr = [$('.img-1'), $('.img-2'), $('.img-3'),];
    var download = $('#download');
    var imgsum = 0;

    //控制图片1内容显示隐藏
    function img1_logo_out() {
        $('.LAELIOAname').fadeOut(500);
        $('.LAELIOAname2').fadeIn(500);
    }

    function img1_logo_in() {
        $('.LAELIOAname').fadeIn(500);
        $('.LAELIOAname2').fadeOut(500);
    }

    //点击切换
    var imgsum1 = 0;
    left.click(function () {
        imgarr[0].css('transform', 'translateX(0%)');
        imgarr[1].css('transform', 'translateX(-100%)')
        img1_logo_in();
        left.fadeOut(100);
        right.fadeIn(100);
        imgsum1 = 0;
        
    });
    right.click(function () {
        imgarr[0].css('transform', 'translateX(-100%)');
        imgarr[1].css('transform', 'translateX(0%)');
        img1_logo_out();
        left.fadeIn(100);
        right.fadeOut(100);
        imgsum1 = 1;
    });
    
    //导航下载按钮功能
    download.click(function() {
        if (imgsum1 == 0) {
        imgarr[0].css('transform', 'translateX(-100%)');
        imgarr[1].css('transform', 'translateX(0%)');
        img1_logo_out();
        left.fadeIn(100);
        right.fadeOut(100);
        imgsum1 = 1;
        }else if (imgsum1 == 1){
        imgsum1 = 0;
        }
    })
    

    //加入服务器提示阅读条列

    //显示
    function error_block() {
        $('#error').fadeIn(300);
    }

    //隐藏
    function error_none() {
        $('#error').fadeOut(300);
    }

    //立即加入按钮显示
    $('.gobutton').click(function () {
        error_block();

    });
    //点击取消时
    $('#no').click(function () {
        error_none();
    });

    
    $('.box-1').hover(function() {
        $(this).find('p').css({'transform':'translateY(-10rem)','display':'none'})

    },function () {
        $(this).find('p').css({'transform':'translateY(0rem)','display':'block'})

    })


})
